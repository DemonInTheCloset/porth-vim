" Vim compiler file
" Compiler:	Porth Reference Implementation
" Maintainer:	DemonInTheCloset <demoninthecloset@gmail.com>
" Last Change:	2022 Jan 25

if exists("current_compiler")
  finish
endif
let current_compiler = "porth"

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo&vim

CompilerSet makeprg=porth\ com\ -r
CompilerSet errorformat=%f:%l:%c:%m,error:%m

let &cpo = s:cpo_save
unlet s:cpo_save
