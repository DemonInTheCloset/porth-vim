# porth-vim

An installable vim plugin for [Porth](https://gitlab.com/tsoding/porth)

## Installing

Use your favorite package manager:

[packer.nvim](https://github.com/wbthomason/packer.nvim)

```lua
use "https://gitlab.com/DemonInTheCloset/porth-vim"
```

[vim-plug](https://github.com/junegunn/vim-plug)

```viml
Plug "https://gitlab.com/DemonInTheCloset/porth-vim"
```
